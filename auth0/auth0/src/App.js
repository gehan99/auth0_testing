
import './App.css';
import Login from './component/login'
// import LogOut from './component/logout'
import Profile from './component/profile'

function App() {
  return (
    <div className='container'>
      <Login/>
      {/* <LogOut/> */}
      <Profile/>
    </div>
  );
}

export default App;
