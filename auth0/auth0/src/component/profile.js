import React from "react";
import { useAuth0 } from '@auth0/auth0-react'
import axios from "axios";

const Profile = () => {
    const { user,getAccessTokenSilently } = useAuth0()

    function callApi(){
        axios.get('http://localhost:3001/user').then(responce=>console.log(responce.data)).
        catch(error=>console.log(error.message))
    }


   async function callProtectedApi(){
    try {
        const token = await getAccessTokenSilently()
        // const sampleToken ='1111111111111111111111'
        var val = 1
        console.log("token-->",token)
        const responce = await axios.get(`http://localhost:3001/product/${val}`,{
            headers:{
                Authorization:`Bearer ${token}`,
            }
        }) 
        console.log("responce--->",responce.data)
    } catch (error) {
        // console.log("responce--->",responce.data)
        console.log("product api-1",error)
    }
        
    }
    
    async function callRoleProtectedApi(){
        try {
            const token = await getAccessTokenSilently()

            const responce = await axios.get("http://localhost:3001/product",{
                headers:{
                    Authorization:`Bearer ${token}`
                }
            })
            console.log(responce.data)
        } catch (error) {
            console.log("product api-2",error)
        }
            // axios.get('http://localhost:3001/user').then(responce=>console.log(responce.data)).catch(error=>console.log(error.message))
        }




    return (
        <>
            <div className="container" style={{ alignItems: "start" }}>
                {console.log(JSON.stringify(user, null, 2))}
            </div>
            <br/>
            <div style={{ alignItems: "center" }}><h3>Add routes</h3></div>
            <div className="container">
                <ul>
                    <li>
                    <button onClick={callApi}>callApi-All user</button>
                    </li>
                    <li>
                    <button onClick={callProtectedApi}>callProtectedApi-product</button>
                    </li>
                    <li>
                    <button onClick={callRoleProtectedApi}>callRoleBaseProtectedApi-all products</button>
                    </li>
                    
                </ul>
            </div>

        </>
    )
}

export default Profile